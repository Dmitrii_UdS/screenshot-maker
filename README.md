This is a program which takes screenshots regularly.  
Period is set by you. Default period is 300 sec.  
You also can set a destination folder for screenshots.  


This program is useful for logging your activity or your sleep time.
